import axios from "axios";

export const API = axios.create({ baseURL: process.env.NEXT_PUBLIC_HOST_API });
// export const API = axios.create({});

export const SetAuthTokenAPI = (token) => {
  API.defaults.headers.common["Authorization"] = `Bearer ${token}`;
};
