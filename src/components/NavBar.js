import styles from "../../styles/navbar.module.css";
import { ReactSearchAutocomplete } from "react-search-autocomplete";
import Link from "next/link";
import movies from "../../src/movies.json";
import { useRouter } from "next/router";

function toItems(movies) {
  let item = [];
  movies.forEach((movie) => {
    item.push({
      id: movie.id,
      name: movie.title,
    });
  });
  return item;
}

export default function NavBar() {
  const items = toItems(movies);
  const router = useRouter();

  const handleOnSearch = (string, results) => {
    console.log(string, results);
  };

  const handleOnSelect = (item) => {
    router.push("/movie/" + item.id);
  };

  const formatResult = (item) => {
    return item;
  };

  return (
    <div className={styles.root}>
      <div className="container">
        <div className={styles.toolbar}>
          <div className={styles.logo}>
            <Link href="/">
              <a>
                <img src="/logo.png" />
              </a>
            </Link>
          </div>
          <div style={{ width: "100%", display: "flex", alignItems: "center", justifyContent: "center" }}>
            <div style={{ width: 400 }}>
              <ReactSearchAutocomplete
                items={items}
                onSearch={handleOnSearch}
                onSelect={handleOnSelect}
                autoFocus
                formatResult={formatResult}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
