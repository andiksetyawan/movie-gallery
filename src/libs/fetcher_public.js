import { API } from "../config/api";

const fetcher = (url) => {
  return API.get(url).then((res) => res.data);
};

export default fetcher;
