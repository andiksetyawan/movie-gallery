import "../styles/globals.css";
import { SWRConfig } from "swr";
import fetcher from "../src/libs/fetcher_public";
import NextNprogress from "nextjs-progressbar";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <NextNprogress color="red" startPosition={0.3} stopDelayMs={200} height={3} showOnShallow={true} />
      <SWRConfig value={{ fetcher }}>
        <Component {...pageProps} />
      </SWRConfig>
    </>
  );
}

export default MyApp;
