import Head from "next/head";
import Link from "next/link";
import React, { useCallback } from "react";

import { BottomScrollListener } from "react-bottom-scroll-listener";
// import React, { useState } from "react";
// import { useSWRInfinite } from "swr";
import useSWRInfinite from "swr/infinite";
import NavBar from "../src/components/NavBar";
import { API } from "../src/config/api";

import styles from "../styles/movie.module.css";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import clsx from "clsx";
import fetcher from "../src/libs/fetcher_public";

import { BsFillHeartFill, BsFillCalendarCheckFill } from "react-icons/bs";
import Moment from "react-moment";

// const getKey = (pageIndex, previousPageData) => {
//   if (previousPageData && !previousPageData.length) return null; // reached the end
//   return `/movie?page=${pageIndex}`; // SWR key
// };

export default function Home({ fallback }) {
  // const [page, setPage] = React.useState(1);
  // const { data: movies, mutate } = useSWRInfinite("/movie?page=" + page, { fallback });

  const [dateRange, setDateRange] = React.useState([null, null]);
  const [startDate, endDate] = dateRange;

  const { data, size, setSize } = useSWRInfinite(
    (pageIndex, previousPageData) => {
      if (previousPageData && !previousPageData.length) return null; // reached the end
      let from = parseInt((startDate?.getTime() / 1000).toFixed(0));
      let until = parseInt((endDate?.getTime() / 1000).toFixed(0));
      if (from && until) {
        return `/movie?page=${pageIndex}&from=${from}&until=${until}`; // SWR key
      }
      return `/movie?page=${pageIndex}`; // SWR key
    },
    fetcher,
    { fallback }
  );

  const DateInput = React.forwardRef(({ value, onClick }, ref) => (
    <input value={value} type="text" onClick={onClick} ref={ref} placeholder="Date Filter" />
  ));

  const handleOnDocumentBottom = () => {
    setSize(size + 1);
  };

  return (
    <div>
      <Head>
        <title>Home : Movie Gallery</title>
        <meta name="description" content="Movie Gallery" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <NavBar />
      <div className="container">
        <div className="row">
          <div className={clsx("col-xs-12", styles.picker)}>
            <div style={{ maxWidth: 250, width: "100%" }}>
              <DatePicker
                selectsRange={true}
                startDate={startDate}
                endDate={endDate}
                onChange={(update) => {
                  setDateRange(update);
                }}
                customInput={<DateInput />}
              />
            </div>
          </div>
          <div className="col-xs-12">
            <div className={styles.list}>
              {data?.map((movies, index) => {
                return movies.map((movie) => (
                  <div key={movie.id}>
                    <Link href={"/movie/" + movie.id}>
                      <a>
                        <div className={styles.listitem} style={{}}>
                          <img src={movie.image}></img>
                          <div className={styles.desc}>
                            <h1>{movie.title}</h1>
                            <div>
                              <span className="badgesmall">
                                <BsFillCalendarCheckFill />
                                <Moment format="DD MMMM YYYY">{movie?.showTime}</Moment>
                              </span>
                              <span className="badgesmall">
                                <BsFillHeartFill />
                                {movie?.like}
                              </span>
                            </div>
                          </div>
                        </div>
                      </a>
                    </Link>
                  </div>
                ));
              })}
            </div>
          </div>
          <BottomScrollListener onBottom={handleOnDocumentBottom} />
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps() {
  //init SSR
  const { data } = await API.get("/movie?page=1");
  return {
    props: {
      fallback: {
        "/movie?page=1": data,
      },
    },
  };
}
