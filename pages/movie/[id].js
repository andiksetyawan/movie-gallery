import Head from "next/head";
import Link from "next/link";
import React, { useCallback } from "react";

import { useRouter } from "next/router";

import NavBar from "../../src/components/NavBar";
import { API } from "../../src/config/api";

import "react-datepicker/dist/react-datepicker.css";
import useSWR from "swr";
import { BsFillHeartFill, BsFillCalendarCheckFill } from "react-icons/bs";

import Moment from "react-moment";

export default function Home({ fallback }) {
  const router = useRouter();
  const { id } = router.query;
  const key = "/movie/" + id;
  console.log("keeyyeydy", key, fallback);
  const { data: movie } = useSWR(key, { fallback });

  return (
    <div>
      <Head>
        <title>{movie?.title} : Movie Gallery</title>
        <meta name="description" content="Movie Gallery" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <NavBar />
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-xs-12">
            <img style={{ borderRadius: 20, width:"100%" }} src={movie?.image} />
          </div>
          <div className="col-md-6 col-xs-12">
            <h1>{movie?.title}</h1>
            <div style={{ marginBottom: 50 }}>
              <span className="badge">
                <BsFillCalendarCheckFill />
                <Moment format="DD MMMM YYYY">{movie?.showTime}</Moment>
              </span>
              <span className="badge">
                <BsFillHeartFill />
                {movie?.like}
              </span>
            </div>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
              scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release
              of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
              like Aldus PageMaker including versions of Lorem Ipsum.
            </p>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
              scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release
              of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
              like Aldus PageMaker including versions of Lorem Ipsum.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps(context) {
  const { id } = context.query;
  //init SSR
  const { data } = await API.get(`/movie/${id}`);
  let payload = {
    props: {
      fallback: {},
    },
  };

  payload.props.fallback[`/movie/${id}`] = data;
  return payload;
}
