// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import movies from "../../../src/movies.json";

export default function handler(req, res) {
  let filtered = movies.filter((x) => x.title.toLowerCase().includes(req.query.q.toLowerCase()));
  res.status(200).json(filtered ?? []);
}
