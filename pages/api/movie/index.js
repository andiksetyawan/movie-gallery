import movies from "../../../src/movies.json";
import moment from "moment";

function paginator(items, current_page, per_page_items) {
  let page = current_page || 1,
    per_page = per_page_items || 8,
    offset = (page - 1) * per_page,
    paginatedItems = items.slice(offset).slice(0, per_page_items),
    total_pages = Math.ceil(items.length / per_page);

  return {
    page: page,
    per_page: per_page,
    pre_page: page - 1 ? page - 1 : null,
    next_page: total_pages > page ? page + 1 : null,
    total: items.length,
    total_pages: total_pages,
    data: paginatedItems,
  };
}

export default function handler(req, res) {
  const { page, from, until } = req.query;

  let mov = movies;
  if (from && until) {
    console.log("filter with date", from, until);
    mov = movies.filter((x) => {
      let d = moment(x.showTime);
      if (d.unix() >= Number(from) && d.unix() <= Number(until)) {
        return true;
      }
      return false;
    });
  }

  let movie = paginator(mov, page, 12);
  res.status(200).json(movie.data);
}
