// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import movies from "../../../src/movies.json";

export default function handler(req, res) {
  let movie = movies.find((x) => x.id === req.query.id);
  res.status(200).json(movie ?? {});
}
